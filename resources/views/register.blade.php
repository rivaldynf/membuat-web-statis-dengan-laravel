<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru</h1>

    <h3>Sign Up Form</h3>

    <!-- Form -->
    <form action="/welcome" method="post">
        @csrf

        <label for="firstname">First name:</label><br><br>
        <input type="text" id="firstname" name="firstname" value="{{ old('firstname') }}"><br><br>
        <label for="lastname">Last name:</label><br><br>
        <input type="text" id="lastname" name="lastname" value="{{ old('lastname') }}"><br><br>

        <label for="gender">Gender:</label><br><br>
        <input type="radio" id="male" name="male" value="{{ old('male') }}">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="female" value="{{ old('female') }}">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="other" value="{{ old('other') }}">
        <label for="other">Other</label><br><br>

        <label for="">Nationality:</label><br><br>
        <select name="nationality" id="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="singapura">Singapura</option>
            <option value="malaysia">Malaysia</option>
            <option value="australia">Australia</option>
        </select><br><br>

        <label for="">Language Spoken:</label><br><br>
        <input type="checkbox" name="indonesia" id="indonesia">Bahasa Indonesia <br>
        <input type="checkbox" name="english" id="english">English <br>
        <input type="checkbox" name="other" id="other">Other <br><br>

        <label for="">Bio:</label><br><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
        
        <input type="submit" value="Submit">
    </form> 
    <!-- Form -->

</body>
</html>